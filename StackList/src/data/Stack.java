package data;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author paper
 */
public class Stack<T> {
    private ArrayList<T> pool = new ArrayList<>();

    public Stack() {
        pool.ensureCapacity(0);
    }
    
    public void push(T o){
        pool.add((T) o);
    }
    public T pop(){
        return pool.remove(pool.size()-1);
    }
    public void sClear(){
        pool.clear();
    }
    public T topEl(){
        return pool.get(pool.size());
    }
    public boolean isSEmtry(){
        return pool.isEmpty();
    }
    public void sDisplay(){
        for (int i = pool.size()-1; i >= 0; i--) {
            System.out.println(pool.get(i));
        }
    }
}
