package data;

//************************  Main.SLLNode.java  *******************************
//           node in a generic singly linked list class
public class SLLNode<T> {
    public T info;
    public SLLNode<T> next;
    public SLLNode() {
        next = null;
    }
    public SLLNode(T el) {
        info = el; next = null;
    }
    public SLLNode(T el, SLLNode<T> ptr) {
        info = el; next = ptr;
    }
}

