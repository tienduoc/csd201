/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.LinkedList;

/**
 *
 * @author tiend
 */
public class Queue<T>{
    private LinkedList<T> lst = new LinkedList();
    
    public void enqueue(T x) {
        lst.addLast(x);
    }
    
    public void clear() {
        lst.clear();
    }
    
    public boolean isEmpty() {
        return lst.isEmpty();
    }
    
    public T dequeue() {
        return lst.removeFirst();
    }
    
    public void firstEl() {
        System.out.println(lst.getFirst());
    }
    
    public void display() {
        for (T t : lst) {
            System.out.println(t);
        }
    }
}
