/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import data.Queue;
import data.Student;

/**
 *
 * @author tiend
 */
public class SapXepSinhVien {
    public static void main(String[] args) {
        Queue<Student> qu1 = new Queue<>();
        Queue<Student> qu2 = new Queue<>();
        
        Student s[] = new Student[7];
        s[0] = new Student("An","Nam",4.0); 
        s[1] = new Student("Anh", "Nu",4.5);
        s[2] = new Student("Thanh","Nu",5.0);
        s[3] = new Student("Hoang","Nam",5.0);
        s[4] = new Student("Long","Nam",6.0);
        s[5] = new Student("Xuan","Nu",7.0);
        s[6] = new Student("Tien","Nam",8.0);
        
        for (int i = 0; i < s.length; i++) {
            if (s[i].getSex().equalsIgnoreCase("Nu")) {
                qu1.enqueue(s[i]);
            }
        }
        
        for (int i = 0; i < s.length; i++) {
            if (s[i].getSex().equalsIgnoreCase("Nam")) {
                qu2.enqueue(s[i]);
            }
        }
        
        qu1.display();
        qu2.display();
    }           
}
