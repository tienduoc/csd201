package main;

import data.Queue;
import java.util.Stack;

/**
 *
 * @author tiend
 */
public class Palindromes {

    public static void main(String[] args) {
        Stack st = new Stack();
        Queue<Integer> qu = new Queue<>();

        String input = "abccba";

        for (int i = 0; i < input.length(); i++) {
            int c = input.charAt(i);
            st.push(c);
            qu.enqueue(c);
        }

        while (!st.isEmpty()) {
            int x = (int) st.pop();
            int y = qu.dequeue();

            if (x != y) {
                System.out.println("NO");
                return;
            }
        }
        System.out.println("YES");
    }
}
