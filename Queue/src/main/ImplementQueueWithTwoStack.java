/*
 * Dùng 2 stack biểu diễn cho 1 cái queue
 */
package main;

import java.util.Stack;

/**
 *
 * @author tiend
 */
public class ImplementQueueWithTwoStack {
    public static Stack st1 = new Stack();
    public static Stack st2 = new Stack();
    
    public static void Enqueue(int ... args) {
        for(int i : args) {
            st1.push(i);
        }
        while (!st1.isEmpty()) {
            st2.push(st1.pop());
        }
    }
    
    public static int Dequeue() {
        return (int)st2.pop();
    }
    
    public static void main(String[] args) {
        Enqueue(0,1,2,3,4,5,6,10,11);
        int x = Dequeue();
        
        System.out.println(x);
    }
}
