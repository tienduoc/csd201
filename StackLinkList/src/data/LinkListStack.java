/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.LinkedList;

/**
 *
 * @author paper
 */
public class LinkListStack<T>{
    private LinkedList<T> pool = new LinkedList();
    
    public void push(T o){
    pool.addFirst(o);
    }
    public T pop(){
        return pool.removeFirst();
    }
    public void sClear(){
        pool.clear();
    }
    public T topEl(){
        return pool.getFirst();
    }
    public boolean isSEmtry(){
        return pool.isEmpty();
    }
    public void sDisplay(){
        for (int i = pool.size()-1; i >= 0; i--) {
            System.out.println(pool.get(i));
        }
    }
}
