/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;
import data.DLL;

/**
 *
 * @author tiend
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("DLL");
        DLL<Double> dll = new DLL<>();
        dll.addToHead(1.1);
        dll.addToTail(1.2);
        dll.printAll();
    }
    
}
