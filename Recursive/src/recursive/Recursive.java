package recursive;

/**
 * @author tiend
 */
public class Recursive {

    public static void print(int n) {
        if (n == 0) {
            print(n);
        } else {
            System.out.println(n);
        }
        print(n - 1);
    }

    public static void main(String[] args) {
        print(4);
    }

}
